package com.example.javademo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserTests {

	@Test
	void userSaysHello() {
    User user = new User();
    assertThat(user.hello()).isEqualTo("Hello");
	}

}
